package com.yen.kb;


import com.google.inject.servlet.ServletModule;

public class ApplicationServletModule extends ServletModule {


    @Override
    protected void configureServlets() {
        bind(FooServlet.class);
        bind(BarServlet.class);
        bind(IndexServlet.class);

        serve("/foo").with(FooServlet.class);
        serve("/bar").with(BarServlet.class);
        serve("/*").with(IndexServlet.class);
    }


}
