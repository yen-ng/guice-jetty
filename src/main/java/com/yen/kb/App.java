package com.yen.kb;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceFilter;
import org.eclipse.jetty.server.DispatcherType;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;

import java.util.EnumSet;

/**
 * Hello world!
 *
 * http://blog.timmattison.com/archives/2014/09/02/full-example-code-showing-how-to-use-guice-and-jetty/
 */



public class App 
{
    public static void main( String[] args ) throws Exception{
        System.out.println( "Hello World!" );

        NonServletModule nonServletModule = new NonServletModule();
        ApplicationServletModule applicationServletModule = new ApplicationServletModule();
        Injector injector = Guice.createInjector(nonServletModule, applicationServletModule);

        int port = 8080;
        Server server = new Server(port);

        ServletContextHandler servletContextHandler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);

        servletContextHandler.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));

    // You MUST add DefaultServlet or your server will always return 404s
        servletContextHandler.addServlet(DefaultServlet.class, "/");

    // Start the server
        server.start();

    // Wait until the server exits
        server.join();

    }
}
